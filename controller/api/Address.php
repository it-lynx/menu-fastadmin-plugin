<?php
/*
 * @Descripttion: 用户收货地址接口控制器
 * @Author: DanceLynx
 * @Date: 2020-11-18 09:38:08
 * @LastEditors: DanceLynx
 * @LastEditTime: 2020-11-18 13:16:36
 */
namespace addons\sdcmenu\controller\api;

use addons\sdcmenu\model\Address as ModelAddress;
use addons\sdcmenu\validate\Address as ValidateAddress;
use think\Validate;

class Address extends Api
{
    protected $noNeedRight = ['*'];
    protected $noNeedLogin = [];
    private $model = null;
    /**
     * @description: 初始化
     */
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new ModelAddress();
    }

    /**
     * @description: 获取当前用户所有收货地址
     * @param string token 用户token
     * @return array response json
     */
    public function index()
    {
        $currentUser = $this->auth->getUser();
        $data = $this->model
            ->where('user_id',$currentUser->id)
            ->order(['is_default'=>'DESC','createtime'=>'DESC'])
            ->paginate();
        $this->success(__('Request success'),$data);
    }

    /**
     * @description: 获取当前用户单个收货地址
     * @param int address_id 收货地址id
     * @param string token 用户token
     * @return array response json
     */
    public function show()
    {
        $currentUser = $this->auth->getUser();
        $address_id = $this->request->request('address_id');
        if(! Validate::checkRule($address_id,'require|number')){
            $this->error(__('id param failed'));
        }
        $data = $this->model
            ->where(['id'=>$address_id,'user_id'=>$currentUser->id])
            ->find();
        $this->success(__('Request success'),$data);
    }

    /**
     * @description: 添加收货地址
     * @param string token 用户token
     * @param string name 姓名
     * @param string mobile 手机号
     * @param string address 详细地址
     * @param string province_name 省级名称
     * @param string city_name 市级名称
     * @param string area_name 区县级名称
     * @param int is_default 是否默认地址(0:否,1:是)
     * @param string latitude 维度
     * @param string longitude 经度
     * @return array response json
     */
    public function add()
    {
        if(! $this->request->isPost()){
            $this->error(__('The request method failed'));
        }
        $data = $this->request->post();
        $validator = new ValidateAddress();
        if(! $validator->check($data)){
            $this->error($validator->getError());
        }
        $currentUser = $this->auth->getUser();
        
        $data['user_id'] = $currentUser->id;
        // 如果当前地址为默认地址，将全部地址设为非默认地址
        if(isset($data['is_default']) && $data['is_default'] == 1){
            $this->model
                ->where('user_id',$currentUser->id)
                ->update(['is_default'=>0]);
        }
        $result = $this->model->allowField(true)->save($data);
        if(! $result){
            $this->error(__('The fail added'));
        }
        $this->success(__('The add successful'));
    }
     /**
     * @description: 修改收货地址
     * @param string token 用户token
     * @param string name 姓名
     * @param string mobile 手机号
     * @param string address 详细地址
     * @param string province_name 省级名称
     * @param string city_name 市级名称
     * @param string area_name 区县级名称
     * @param int is_default 是否默认地址(0:否,1:是)
     * @param string latitude 维度
     * @param string longitude 经度
     * @return array response json
     */
    public function edit()
    {
        if(! $this->request->isPost()){
            $this->error(__('The request method must is Post'));
        }
        $data = $this->request->post();
        $validator = new ValidateAddress();
        if(! $validator->check($data)){
            $this->error($validator->getError());
        }
        $currentUser = $this->auth->getUser();
        // 如果当前地址为默认地址，将全部地址设为非默认地址
        if(isset($data['is_default']) && $data['is_default'] == 1){
            $this->model
                ->where('user_id',$currentUser->id)
                ->update(['is_default'=>0]);
        }
        $result = $this->model
            ->isUpdate(true)
            ->save($data,['user_id'=>$currentUser->id,'id'=>$data['id']]);
        if(! $result){
            $this->error(__('The update failed'));
        }
        $this->success(__('The update successful'));
    }

    /**
     * @description: 删除收货地址
     * @param string token 用户token
     * @param int id 收货地址id
     * @return array response json
     */
    public function del()
    {
        $id = $this->request->request('id');
        if(! Validate::checkRule($id,'require|number')){
            $this->error(__('The id param require and number'));
        }
        $currentUser = $this->auth->getUser();
        $result = $this->model
            ->where(['user_id'=>$currentUser->id,'id'=>$id])
            ->delete();
        if(! $result){
            $this->error(__('The delete is failed'));
        }
        $this->success(__('The delete successful'));
    }
}